﻿namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.But_Open = new System.Windows.Forms.Button();
            this.TxtSeriarPort = new System.Windows.Forms.TextBox();
            this.TxtResponse = new System.Windows.Forms.TextBox();
            this.ButInteraction = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.Check_NetworkPort = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButContinuousReading = new System.Windows.Forms.Button();
            this.TxtLength = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ComFunction = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // But_Open
            // 
            this.But_Open.Location = new System.Drawing.Point(159, 92);
            this.But_Open.Name = "But_Open";
            this.But_Open.Size = new System.Drawing.Size(75, 23);
            this.But_Open.TabIndex = 0;
            this.But_Open.Text = "打 开";
            this.But_Open.UseVisualStyleBackColor = true;
            this.But_Open.Click += new System.EventHandler(this.But_Open_Click);
            // 
            // TxtSeriarPort
            // 
            this.TxtSeriarPort.Location = new System.Drawing.Point(159, 32);
            this.TxtSeriarPort.Name = "TxtSeriarPort";
            this.TxtSeriarPort.Size = new System.Drawing.Size(143, 23);
            this.TxtSeriarPort.TabIndex = 1;
            this.TxtSeriarPort.Text = "COM2";
            // 
            // TxtResponse
            // 
            this.TxtResponse.Location = new System.Drawing.Point(12, 144);
            this.TxtResponse.Multiline = true;
            this.TxtResponse.Name = "TxtResponse";
            this.TxtResponse.Size = new System.Drawing.Size(730, 586);
            this.TxtResponse.TabIndex = 2;
            // 
            // ButInteraction
            // 
            this.ButInteraction.Location = new System.Drawing.Point(334, 92);
            this.ButInteraction.Name = "ButInteraction";
            this.ButInteraction.Size = new System.Drawing.Size(75, 23);
            this.ButInteraction.TabIndex = 4;
            this.ButInteraction.Text = "交 互";
            this.ButInteraction.UseVisualStyleBackColor = true;
            this.ButInteraction.Click += new System.EventHandler(this.ButInteraction_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtPort);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.But_Open);
            this.groupBox1.Controls.Add(this.Check_NetworkPort);
            this.groupBox1.Controls.Add(this.TxtSeriarPort);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 126);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "配 置";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "端口或波特率";
            // 
            // TxtPort
            // 
            this.TxtPort.Location = new System.Drawing.Point(159, 63);
            this.TxtPort.Name = "TxtPort";
            this.TxtPort.Size = new System.Drawing.Size(100, 23);
            this.TxtPort.TabIndex = 3;
            this.TxtPort.Text = "9600";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "IP或串口";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(22, 61);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(51, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "串口";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // Check_NetworkPort
            // 
            this.Check_NetworkPort.AutoSize = true;
            this.Check_NetworkPort.Location = new System.Drawing.Point(22, 34);
            this.Check_NetworkPort.Name = "Check_NetworkPort";
            this.Check_NetworkPort.Size = new System.Drawing.Size(51, 21);
            this.Check_NetworkPort.TabIndex = 0;
            this.Check_NetworkPort.Text = "网口";
            this.Check_NetworkPort.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButContinuousReading);
            this.groupBox2.Controls.Add(this.TxtLength);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.TxtValue);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.TxtAddress);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.ComFunction);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.ButInteraction);
            this.groupBox2.Location = new System.Drawing.Point(326, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(416, 126);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "功 能 测 试";
            // 
            // ButContinuousReading
            // 
            this.ButContinuousReading.Location = new System.Drawing.Point(335, 27);
            this.ButContinuousReading.Name = "ButContinuousReading";
            this.ButContinuousReading.Size = new System.Drawing.Size(75, 23);
            this.ButContinuousReading.TabIndex = 13;
            this.ButContinuousReading.Text = "开 始";
            this.ButContinuousReading.UseVisualStyleBackColor = true;
            this.ButContinuousReading.Click += new System.EventHandler(this.ButContinuousReading_Click);
            // 
            // TxtLength
            // 
            this.TxtLength.Location = new System.Drawing.Point(66, 92);
            this.TxtLength.Name = "TxtLength";
            this.TxtLength.Size = new System.Drawing.Size(100, 23);
            this.TxtLength.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "长度";
            // 
            // TxtValue
            // 
            this.TxtValue.Location = new System.Drawing.Point(181, 26);
            this.TxtValue.Multiline = true;
            this.TxtValue.Name = "TxtValue";
            this.TxtValue.Size = new System.Drawing.Size(137, 89);
            this.TxtValue.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(143, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "数据";
            // 
            // TxtAddress
            // 
            this.TxtAddress.Location = new System.Drawing.Point(66, 62);
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.Size = new System.Drawing.Size(100, 23);
            this.TxtAddress.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "起始地址";
            // 
            // ComFunction
            // 
            this.ComFunction.FormattingEnabled = true;
            this.ComFunction.Items.AddRange(new object[] {
            "0x01",
            "0x02",
            "0x03",
            "0x04",
            "0x05",
            "0x06",
            "0x0F",
            "0x10"});
            this.ComFunction.Location = new System.Drawing.Point(66, 27);
            this.ComFunction.Name = "ComFunction";
            this.ComFunction.Size = new System.Drawing.Size(66, 25);
            this.ComFunction.TabIndex = 6;
            this.ComFunction.Text = "0x01";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "功能码";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 742);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TxtResponse);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modbus Rtu测试工具";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button But_Open;
        private System.Windows.Forms.TextBox TxtSeriarPort;
        private System.Windows.Forms.TextBox TxtResponse;
        private System.Windows.Forms.Button ButInteraction;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox Check_NetworkPort;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ComFunction;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtLength;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ButContinuousReading;
    }
}
