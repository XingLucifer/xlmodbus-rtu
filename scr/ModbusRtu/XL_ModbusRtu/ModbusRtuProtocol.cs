﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_ModbusRtu
{
    public class ModbusRtuProtocol
    {
        private byte address;
        public ModbusRtuProtocol(byte address)
        {
            this.address = address;
        }

        public bool Verify(IList<byte> bytes)
        {
            if (bytes == null ||  bytes.Count < 2)
            {
                return false;
            }

            return true;
        }

        public byte[] Serialize(Enums.ModbusEnums enums, short address, short length)
        {
            byte[] bytes = new byte[length];
            return default;
        }

        public byte[] Deserialize(IList<byte> data)
        {
            return default;
        }
    }
}
