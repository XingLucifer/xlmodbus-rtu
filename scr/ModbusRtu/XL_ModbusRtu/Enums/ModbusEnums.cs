﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_ModbusRtu.Enums
{
    public enum ModbusEnums : byte
    {
        /// <summary>
        /// 读线圈状态-位操作
        /// </summary>
        READ_COIL_STATUS = 0x01,
        /// <summary>
        /// 读离散输入状态-位操作
        /// </summary>
        READ_INPUT_STATUS,
        /// <summary>
        /// 读保持寄存器-字操作
        /// </summary>
        READ_HOLDING_REGISTER,
        /// <summary>
        /// 读输入寄存器-字操作
        /// </summary>
        READ_INPUT_REGISTER,
        /// <summary>
        /// 写线圈状态-位操作
        /// </summary>
        WRITE_SINGLE_COIL,
        /// <summary>
        /// 写单个保持寄存器-字操作
        /// </summary>
        WRITE_SINGLE_REGISTER,
        /// <summary>
        /// 写多个线圈-位操作
        /// </summary>
        WRITE_MULTIPLE_COIL = 15,
        /// <summary>
        /// 写多个保持寄存器-字操作
        /// </summary>
        WRITE_MULTIPLE_REGISTER = 16,
    }
}
