﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_ModbusRtu.Enums
{
    public enum MudbosErrorCode : byte
    {
        非法功能 = 0x01,
        非法数据地址 = 0x02,
        非法数据值,
        从站设备故障,
        确认,
        从属设备忙,
        存储奇偶差错 = 0x08,
        不可用网关路径 = 0x0a,
        网关目标设备响应失败 = 0x0b,
    }
}
