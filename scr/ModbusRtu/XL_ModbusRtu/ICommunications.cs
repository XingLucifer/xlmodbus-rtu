﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XL_ModbusRtu
{
    public interface ICommunications
    {
        /// <summary>
        /// 设备IP或串口
        /// </summary>
        string IP { get; }
        /// <summary>
        /// 端口或波特率
        /// </summary>
        int Port { get; }
        /// <summary>
        /// 索引
        /// </summary>
        int Index { get; }
        /// <summary>
        /// 超时时间
        /// </summary>
        int Timeout { get; set; }
        void Write(byte[] bytes);
        byte[] Read();
        bool Open();
        void Close();
    }
}
