# XLModbusRtu

#### 介绍
该库可用于网口下的modbus rtu协议

#### 库环境

.net 5 winfrom


#### 使用说明

1.  该库用法与modbus tcp库 一模（mu）一样（PS：详情请运行demo测试）

2. 需要注意的是该库不具备多线程安全，库内也不捕获异常

#### 群聊

![微信群聊](https://gitee.com/XingLucifer/XMFins/raw/master/image/wx.jpg)
